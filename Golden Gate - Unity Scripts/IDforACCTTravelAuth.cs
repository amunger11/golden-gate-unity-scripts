﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hyland.Unity;
using System.Data;
using Oracle.DataAccess.Client;

namespace UnityScripts
{
    public class IDforACCTTravelAuth : Hyland.Unity.IExternalAutofillKeysetScript
    {
        private Hyland.Unity.Application _app;

        private const string ConnectionString = @"Driver={Microsoft ODBC for Oracle};
                    CONNECTSTRING=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
                    (HOST=172.16.36.42)(PORT=1521))
                    (CONNECT_DATA=(SERVICE_NAME=ggbprod)));uid=ggbro;pwd=ifas13ro;";

        private const string query = @"
                SELECT 
                    e.id,
                    name,
                    substr(d.gl_key,1,3),
                    long_desc,
                    substr(d.gl_key,1,6),
                    phone_cd,
                    phone_cd2,
                    phone_no,
                    phone_no2
                FROM 
                    hr_empmstr e,
                    hr_emppay p,
                    hr_earndist d,
                    hr_pcntble pcn
                WHERE 
                    e.id = :primaryKeyword
                    AND p.pay_beg <= sysdate 
                    AND p.pay_end >= sysdate
                    AND e.id = p.id
                    AND p.uniqueid = d.uniqueid
                    AND p.position = pcn.position
                    AND p.pcn = pcn.pcn";

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                _app = app;
                app.Diagnostics.Write("Starting ID for ACCT Travel Auth Script...");

                KeywordTypeList autofillKeywordTypes = args.KeywordTypes; //all keyword types in autofill
                Keyword primary = args.PrimaryKeyword; //primary keyword of the autofill
                ExternalAutofillKeysetEventResults results = args.Results; //result object to pass back to the client

                //Note - each keyword must be added to the result set - even the primary keyword) 
                ReturnResult(results, primary, autofillKeywordTypes);

            }
            catch (Exception ex)
            {
                app.Diagnostics.Write(ex.Message);
                app.Diagnostics.Write(ex.StackTrace);
            }
        }

        /// <summary>
        /// Method that populates the results object. Will set the secondary values.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="keywordTypes"></param>
        private void ReturnResult(ExternalAutofillKeysetEventResults results, Keyword primaryKeyword, KeywordTypeList keywordTypes)
        {
            List<string> KeywordList = new List<string>() { "Project #", "Department #", "Position Title", "GL Key", "Phone Number" };

            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        cmd.Parameters.Add(new OracleParameter("primaryKeyword", primaryKeyword.Value));
                        cmd.CommandType = CommandType.Text;

                        using (OracleDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr == null)
                                throw new ApplicationException("Data reader could not be established");

                            ExternalAutofillKeysetData data = results.CreateExternalAutofillKeysetData();

                            while (dr.Read())
                            {
                                results.AddExternalKeywordAutofillKeysetData(BuildAutofill(data, dr, primaryKeyword, KeywordList));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if ((_app != null) && (_app.Diagnostics != null))
                {
                    _app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        _app.CurrentUser.Name));

                    _app.Diagnostics.Write(ex);
                    _app.Diagnostics.Write(ex.StackTrace);
                }
            }
            finally
            {
                _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "CSTS - Case Based Autofill (No Pending)"));
                _app.Disconnect();
            }
        }
        #endregion

        #region Helpher Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dr"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="KeywordList"></param>
        /// <returns></returns>
        private ExternalAutofillKeysetData BuildAutofill(ExternalAutofillKeysetData data, OracleDataReader dr, Keyword primaryKeyword, List<string> KeywordList)
        {
            data.SetKeyword(primaryKeyword);

            Keyword key = null;

            for (int i = 0; i < KeywordList.Count; i++)
            {
                if (dr.GetValue(i + 1) != null)
                {
                    KeywordType keyType = _app.Core.KeywordTypes.Find(KeywordList[i]);
                    if (keyType == null)
                        throw new Exception("Could not find keyword type " + KeywordList[i] + " in the keyword type list for this autofill.");

                    if (KeywordList[i] == "Phone Number")
                    {
                        if(dr.GetValue(5).ToString().Trim() == "OF")
                        {
                            key = keyType.CreateKeyword(dr.GetValue(7).ToString());
                        }
                        else if (dr.GetValue(6).ToString().Trim() == "OF")
                        {
                            key = keyType.CreateKeyword(dr.GetValue(8).ToString());
                        }
                        else
                        {
                            key = keyType.CreateKeyword("");
                        }
                    }
                    else
                    {
                        key = keyType.CreateKeyword(dr.GetValue(i + 1).ToString());
                    }

                    data.SetKeyword(key);
                }

            }
            

            return data;
        }

        #endregion
    }
}

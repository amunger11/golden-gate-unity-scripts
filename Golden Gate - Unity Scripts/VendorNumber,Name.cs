﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hyland.Unity;
using System.Data;
using Oracle.DataAccess.Client;

namespace UnityScripts
{
    class VendorNumber_Name : Hyland.Unity.IExternalAutofillKeysetScript
    {
        private Hyland.Unity.Application _app;

        const string ConnectionString = @"Driver={Microsoft ODBC for Oracle};
                    CONNECTSTRING=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
                    (HOST=172.16.36.42)(PORT=1521))
                    (CONNECT_DATA=(SERVICE_NAME=ggbprod))); uid=bsi;pwd=kam;";

        const string query = @"
                SELECT
                    PE_NAME 
                FROM
                    PE_NAME_MSTR 
                WHERE
                    PE_ID = :primaryKeyword";

        private const string VendorName = "Vendor Name";

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                _app = app;
                app.Diagnostics.Write("Starting Vendor Number, Name Script...");

                KeywordTypeList autofillKeywordTypes = args.KeywordTypes; //all keyword types in autofill
                Keyword primary = args.PrimaryKeyword; //primary keyword of the autofill
                ExternalAutofillKeysetEventResults results = args.Results; //result object to pass back to the client

                //Note - each keyword must be added to the result set - even the primary keyword) 
                ReturnResult(results, primary, autofillKeywordTypes);

            }
            catch (Exception ex)
            {
                app.Diagnostics.Write(ex.Message);
                app.Diagnostics.Write(ex.StackTrace);
            }
        }

        /// <summary>
        /// Method that populates the results object. Will set the secondary values.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="keywordTypes"></param>
        private void ReturnResult(ExternalAutofillKeysetEventResults results, Keyword primaryKeyword, KeywordTypeList keywordTypes)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        cmd.Parameters.Add(new OracleParameter("primaryKeyword", primaryKeyword.Value));
                        cmd.CommandType = CommandType.Text;

                        using (OracleDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr == null)
                                throw new ApplicationException("Data reader could not be established");

                            ExternalAutofillKeysetData data = results.CreateExternalAutofillKeysetData();

                            while (dr.Read())
                            {
                                results.AddExternalKeywordAutofillKeysetData(BuildAutofill(data, dr, primaryKeyword));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if ((_app != null) && (_app.Diagnostics != null))
                {
                    _app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        _app.CurrentUser.Name));

                    _app.Diagnostics.Write(ex);
                    _app.Diagnostics.Write(ex.StackTrace);
                }
            }
            finally
            {
                _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "CSTS - Case Based Autofill (No Pending)"));
                _app.Disconnect();
            }
        }
        #endregion

        #region Helpher Methods

        private ExternalAutofillKeysetData BuildAutofill(ExternalAutofillKeysetData data, OracleDataReader dr, Keyword primaryKeyword)
        {
            data.SetKeyword(primaryKeyword);

            Keyword key = null;

            KeywordType vendNameKT = _app.Core.KeywordTypes.Find(VendorName);
            if (vendNameKT == null)
                throw new Exception(string.Format("Could not find keyword type [{0}] in the keyword type list for this autofill.", VendorName));

            if (dr.GetValue(0) != null)
            {
                key = vendNameKT.CreateKeyword(dr.GetValue(0).ToString());
                data.SetKeyword(key);
            }
            else
            {
                key = vendNameKT.CreateKeyword("Invalid/Inactive");
                data.SetKeyword(key);
            }

            return data;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hyland.Unity;
using System.Data.SqlClient;
using System.Data;

namespace UnityScripts
{
    class ID_Name_Dept_Pos_GLKey_Update : Hyland.Unity.IExternalAutofillKeysetScript
    {
        private Hyland.Unity.Application _app;
        private Keyword _primaryKey;
        private ExternalAutofillKeysetEventResults _results;
        private KeywordTypeList _keywordTypes;

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                _app = app;
                app.Diagnostics.Write("Starting ID for ACCT Travel Auth Script...");
                
                _keywordTypes = args.KeywordTypes; //all keyword types in autofill
                _primaryKey  = args.PrimaryKeyword; //primary keyword of the autofill
                 _results  = args.Results; //result object to pass back to the client

       
                //Note - each keyword must be added to the result set - even the primary keyword) 
                //TODO: Make the decision to go global or not.  if Global, dont use locals that you set globals with and you wouldnt need to pass the globals in
                //to the method.  If you go local the pass them into the method.
                //
                 ReturnResult(_results, _primaryKey, _keywordTypes);

            }
            catch (Exception ex)
            {
                app.Diagnostics.Write(ex.Message);
                app.Diagnostics.Write(ex.StackTrace);
            }
        }

        /// <summary>
        /// Method that populates the results object. Will set the secondary values.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="keywordTypes"></param>
        private void ReturnResult(ExternalAutofillKeysetEventResults results, Keyword primaryKeyword, KeywordTypeList keywordTypes)
        {
            List<string> KeywordList = new List<string>() { "Employee Name", "Department #", "Position Title", "GL Key" };
            //TODO: Move to the top of the script and make them global.  For easy update if/when needed.
            //Oracle connnection string
            //const string sConString = @"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=agp-ebsdb-test.agp.com)(PORT=1528))(CONNECT_DATA=(SID=PATCHORA))); User Id=Username;Password=password;";
            const string ConnectionString = @"Driver={Microsoft ODBC for Oracle}; 
                    CONNECTSTRING=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
                    (HOST=172.16.36.42)(PORT=1521))
                    (CONNECT_DATA=(SERVICE_NAME=ggbprod))); uid=bsi;pwd=kam;";
            //TODO: Move to the top of the script and make them global.  For easy update if/when needed.
            //TODO: Oracle use : rather tahn @ for parameters
            const string query = @"
                SELECT 
                    e.id,name,
                    substr(d.gl_key,1,3),
                    long_desc,
                    substr(d.gl_key,1,6)
                FROM 
                    hr_empmstr e,
                    hr_emppay p,
                    hr_earndist d,
                    hr_pcntble pcn
                WHERE 
                    e.id = @primaryKeyword
                    AND p.calc_beg<=sysdate 
                    AND p.calc_end >= sysdate
                    AND e.id = p.id
                    AND p.uniqueid = d.uniqueid
                    AND p.position = pcn.position
                    AND p.pcn = pcn.pcn
                    AND stat = 'A' 
                    AND hr_status = 'A'";

            try
            {
                //TODO: change to OracleConnection
                using (SqlConnection conn = (SqlConnection)_app.Configuration.GetConnection(ConnectionString))
                {
                    conn.Open();

                    //TODO:  Needs to be disposed by using or try catch
                    //TODO: Change to OracleCommand
                    SqlCommand cmd = new SqlCommand(query, conn);
                    
                    cmd.Parameters.Add(new SqlParameter("@primaryKeyword", primaryKeyword.Value));
                    cmd.CommandType = CommandType.Text;
                    //TODO:  Needs to be disposed by using or try catch
                    //TODO: Change to OracleDataReader and loop through rows
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    while (true)
                    {
                        //Add the data to the results
                        results.AddExternalKeywordAutofillKeysetData(GetData(dr, KeywordList));
                    }
                   
                }
            }
            catch (Exception ex)
            {
                if ((_app != null) && (_app.Diagnostics != null))
                {
                    _app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        _app.CurrentUser.Name));

                    _app.Diagnostics.Write(ex);
                    _app.Diagnostics.Write(ex.StackTrace);
                }
            }
            finally
            {
                _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "CSTS - Case Based Autofill (No Pending)"));
                _app.Disconnect();
            }
        }
        #endregion

        #region Helper Methods
        //TODO: this whole thing is going to change when you switch to ORacle
        private ExternalAutofillKeysetData GetData(SqlDataAdapter adapter, List<string> KeywordList)
        {
            ExternalAutofillKeysetData data = _results.CreateExternalAutofillKeysetData();
            data.SetKeyword(_primaryKey);
            
            DataTable dt = new DataTable();
            DataSet dataSet = new DataSet();

            adapter.Fill(dataSet);

            dt = dataSet.Tables[0];

            Keyword key = null;

            if (dt != null)
            {
                DataRow row = dt.Rows[0];
                for (int i = 0; i < KeywordList.Count; i++)
                {
                    if (row[i + 1] != null)
                    {
                        KeywordType keyType = _keywordTypes.Find(KeywordList[i]);
                        if (keyType == null)
                            throw new Exception("Could not find keyword type " + KeywordList[i] + " in the keyword type list for this autofill.");

                        key = keyType.CreateKeyword(row[i + 1].ToString());

                        data.SetKeyword(key);
                    }

                }
            }
            else
            {
                foreach (var item in KeywordList)
                {
                    KeywordType keyType = _keywordTypes.Find(item);
                    if (keyType == null)
                    {
                        throw new Exception("Could not find keyword type " + item + " in the keyword type list for this autofill.");
                    }

                    key = keyType.CreateKeyword("");
                    data.SetKeyword(key);
                }
            }

            return data;
        }

        #endregion
    }
}

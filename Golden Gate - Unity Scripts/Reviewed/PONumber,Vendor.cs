﻿//TODO: Remove any references not needed.  I am guilty of leaving references too.  Should only expose what is needed.
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hyland.Unity;
//TODO:You do not need the sqlcleint for Oracle.
using System.Data.SqlClient;
using System.Data;
using Oracle.DataAccess.Client;

namespace UnityScripts
{
    class PONumber_Vendor_Reviewed : Hyland.Unity.IExternalAutofillKeysetScript
    {
        private Hyland.Unity.Application _app;

        private const string ConnectionString = @"Driver={Microsoft ODBC for Oracle};
                    CONNECTSTRING=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
                    (HOST=172.16.36.42)(PORT=1521))
                    (CONNECT_DATA=(SERVICE_NAME=ggbprod))); uid=bsi;pwd=kam;";

        private const string query = @"
                SELECT
                    pe_id, 
                    pe_name 
                FROM
                    pe_name_mstr pe, 
                    pop_pv_dtl po
                WHERE
                    pe.pe_id = po.pop_pe_id 
                    AND pop_po_no = :primaryKeyword";

        private const string VendorNum = "Vendor #";
        private const string VendorName = "Vendor Name";

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                _app = app;
                app.Diagnostics.Write("Starting PO Number, Vendor Script...");

                KeywordTypeList autofillKeywordTypes = args.KeywordTypes; //all keyword types in autofill
                Keyword primary = args.PrimaryKeyword; //primary keyword of the autofill
                ExternalAutofillKeysetEventResults results = args.Results; //result object to pass back to the client

                //Note - each keyword must be added to the result set - even the primary keyword) 
                ReturnResult(results, primary, autofillKeywordTypes);

            }
            catch (Exception ex)
            {
                app.Diagnostics.Write(ex.Message);
                app.Diagnostics.Write(ex.StackTrace);
            }
        }

        /// <summary>
        /// Method that populates the results object. Will set the secondary values.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="keywordTypes"></param>
        private void ReturnResult(ExternalAutofillKeysetEventResults results, Keyword primaryKeyword, KeywordTypeList keywordTypes)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    { 
                        cmd.Parameters.Add("primaryKeyword", primaryKeyword.Value);
                        cmd.CommandType = CommandType.Text;

                        using (OracleDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr == null)
                                throw new ApplicationException("Data reader could not be established");

                            while (dr.Read())
                            {
                                results.AddExternalKeywordAutofillKeysetData(BuildAutofill(results, dr, primaryKeyword));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if ((_app != null) && (_app.Diagnostics != null))
                {
                    _app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        _app.CurrentUser.Name));

                    _app.Diagnostics.Write(ex);
                    _app.Diagnostics.Write(ex.StackTrace);
                }
            }
            finally
            {
                _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "Starting PO Number, Vendor"));
            }
        }
        #endregion

        #region Helpher Methods

        private ExternalAutofillKeysetData BuildAutofill(ExternalAutofillKeysetEventResults results, OracleDataReader dr, Keyword primaryKeyword)
        {
            ExternalAutofillKeysetData data = results.CreateExternalAutofillKeysetData();
            data.SetKeyword(primaryKeyword);

            Keyword key1 = null;
            Keyword key2 = null;

            KeywordType vendNumKT = _app.Core.KeywordTypes.Find(VendorNum);
            if (vendNumKT == null)
                throw new Exception(string.Format("Could not find keyword type [{0}] in the keyword type list for this autofill.", VendorNum));

            KeywordType vendNameKT = _app.Core.KeywordTypes.Find(VendorName);
            if (vendNameKT == null)
                throw new Exception(string.Format("Could not find keyword type [{0}] in the keyword type list for this autofill.", VendorName));

            if (dr.GetValue(1) != null)
            {
                key1 = CreateKeywordHelper(vendNumKT, dr.GetValue(1).ToString());
            }
            else
            {
                key1 = CreateKeywordHelper(vendNumKT, "");
            }

            if (dr.GetValue(2) != null)
            {
                key2 = CreateKeywordHelper(vendNameKT, dr.GetValue(2).ToString());
            }
            else
            {
                key2 = CreateKeywordHelper(vendNameKT, "");
            }
            data.SetKeyword(key1);
            data.SetKeyword(key2);

            return data;
        }

        private Keyword CreateKeywordHelper(KeywordType Keytype, string Value)
        {
            Keyword key = null;
            switch (Keytype.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    decimal decVal = decimal.Parse(Value);
                    key = Keytype.CreateKeyword(decVal);
                    break;
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:
                    DateTime dateVal = DateTime.Parse(Value);
                    key = Keytype.CreateKeyword(dateVal);
                    break;
                case KeywordDataType.FloatingPoint:
                    double dblVal = double.Parse(Value);
                    key = Keytype.CreateKeyword(dblVal);
                    break;
                case KeywordDataType.Numeric9:
                    long lngVal = long.Parse(Value);
                    key = Keytype.CreateKeyword(lngVal);
                    break;
                default:
                    key = Keytype.CreateKeyword(Value);
                    break;
            }
            return key;
        }

        #endregion
    }
}

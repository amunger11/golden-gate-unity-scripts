﻿//TODO: Remove any references not needed.  I am guilty of leaving references too.  Should only expose what is needed.
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hyland.Unity;
//TODO:You do not need the sqlcleint for Oracle.
using System.Data.SqlClient;
using System.Data;
using Oracle.DataAccess.Client;

namespace UnityScripts
{
    class JLKey_Desc_Reviewed : Hyland.Unity.IExternalAutofillKeysetScript
    {
        private Hyland.Unity.Application _app;

        private const string ConnectionString = @"Driver={Microsoft ODBC for Oracle};
                    CONNECTSTRING=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
                    (HOST=172.16.36.42)(PORT=1521))
                    (CONNECT_DATA=(SERVICE_NAME=ggbprod))); uid=bsi;pwd=kam;";

        private const string query = @"
                SELECT
                    glk_title_dl
                FROM
                    glk_key_mstr
                WHERE
                    glk_key = :primaryKeyword
                    AND glk_gr = 'JL'
                    AND glk_grp_part03 = '0000'
                    AND glk_stat = 'A'";

        private const string ProjectNameKT = "Project Name";

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                _app = app;
                app.Diagnostics.Write("Starting JL Key, Desc Script...");

                KeywordTypeList autofillKeywordTypes = args.KeywordTypes; //all keyword types in autofill
                Keyword primary = args.PrimaryKeyword; //primary keyword of the autofill
                ExternalAutofillKeysetEventResults results = args.Results; //result object to pass back to the client

                //Note - each keyword must be added to the result set - even the primary keyword) 
                ReturnResult(results, primary, autofillKeywordTypes);

            }
            catch (Exception ex)
            {
                app.Diagnostics.Write(ex.Message);
                app.Diagnostics.Write(ex.StackTrace);
            }
        }

        /// <summary>
        /// Method that populates the results object. Will set the secondary values.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="keywordTypes"></param>
        private void ReturnResult(ExternalAutofillKeysetEventResults results, Keyword primaryKeyword, KeywordTypeList keywordTypes)
        {
            try
            {
                using (OracleConnection conn = new OracleConnection(ConnectionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand(query, conn))
                    {
                        cmd.Parameters.Add("primaryKeyword", primaryKeyword.Value);
                        cmd.CommandType = CommandType.Text;

                        using (OracleDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr == null)
                                throw new ApplicationException("Data reader could not be established");

                            while (dr.Read())
                            {
                                results.AddExternalKeywordAutofillKeysetData(BuildAutofill(results, dr, primaryKeyword));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if ((_app != null) && (_app.Diagnostics != null))
                {
                    _app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        _app.CurrentUser.Name));

                    _app.Diagnostics.Write(ex);
                    _app.Diagnostics.Write(ex.StackTrace);
                }
            }
            finally
            {
                _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "Starting JL Key, Desc"));
            }
        }
        #endregion

        #region Helpher Methods

        private ExternalAutofillKeysetData BuildAutofill(ExternalAutofillKeysetEventResults results, OracleDataReader dr, Keyword primaryKeyword)
        {
            ExternalAutofillKeysetData data = results.CreateExternalAutofillKeysetData();
            data.SetKeyword(primaryKeyword);

            Keyword key = null;
            KeywordType keyType = _app.Core.KeywordTypes.Find(ProjectNameKT);

            if (keyType == null)
                throw new Exception(string.Format("Could not find keyword type [{0}] in the keyword type list for this autofill.", ProjectNameKT));

            if (dr.GetValue(1) != null)
            {
                if (dr.GetValue(0) == "")
                {
                    key = CreateKeywordHelper(keyType, "Invalid/Inactive");
                }
                else
                {
                    key = CreateKeywordHelper(keyType, dr.GetValue(1).ToString());
                }

                data.SetKeyword(key);
            }

            return data;
        }

        private Keyword CreateKeywordHelper(KeywordType Keytype, string Value)
        {
            Keyword key = null;
            switch (Keytype.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    decimal decVal = decimal.Parse(Value);
                    key = Keytype.CreateKeyword(decVal);
                    break;
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:
                    DateTime dateVal = DateTime.Parse(Value);
                    key = Keytype.CreateKeyword(dateVal);
                    break;
                case KeywordDataType.FloatingPoint:
                    double dblVal = double.Parse(Value);
                    key = Keytype.CreateKeyword(dblVal);
                    break;
                case KeywordDataType.Numeric9:
                    long lngVal = long.Parse(Value);
                    key = Keytype.CreateKeyword(lngVal);
                    break;
                default:
                    key = Keytype.CreateKeyword(Value);
                    break;
            }
            return key;
        }

        #endregion
    }
}

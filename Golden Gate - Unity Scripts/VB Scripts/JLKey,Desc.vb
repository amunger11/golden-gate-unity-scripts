Option Explicit On

'Used by Capital And Grants - Budget Revision Form

Dim FM_Conn
Dim FM_CMD
Dim FM_RST

Sub OracleClose()
FM_Conn.Close
Set FM_Conn = Nothing
Set FM_CMD = Nothing
End Sub

Sub OracleConnect()

FM_Conn.ConnectionString = "Driver={Microsoft ODBC for Oracle}; " & _
            "CONNECTSTRING=(DESCRIPTION=" & _
            "(ADDRESS=(PROTOCOL=TCP)" & _
            "(HOST=172.16.36.42)(PORT=1521))" & _
            "(CONNECT_DATA=(SERVICE_NAME=ggbprod))); uid=bsi;pwd=kam;"

FM_Conn.Open
FM_CMD.ActiveConnection = FM_Conn

End Sub

Sub GetKeywordSetRecords(PrimaryValue, KeysetDef, Results)

set FM_Conn = createobject("ADODB.Connection")
set FM_CMD = createobject("ADODB.Command")
set FM_RST = createobject("ADODB.Recordset")
OracleConnect

    FM_CMD.CommandText = "select glk_key,glk_title_dl" & _
			 " from glk_key_mstr" & _
			 " where glk_key = '" & PrimaryValue & "'" & _
			 " and glk_gr='JL' and glk_grp_part03='0000'" & _
			 " and glk_stat='A'"
    Set FM_RST = FM_CMD.Execute
    if FM_RST.eof then
	Call Results.beginrow
	Call Results.adddata("Project #",trim(PrimaryValue))
	call Results.adddata("Project Name","Invalid/Inactive")
	call Results.endrow
    else
    	Call Results.beginrow
    	Call Results.adddata("Project #", trim(PrimaryValue))
    	Call Results.adddata("Project Name", trim(FM_RST.fields(1)))
    	Call Results.endrow
    end if

FM_RST.Close
set FM_RST=nothing

OracleClose

End Sub

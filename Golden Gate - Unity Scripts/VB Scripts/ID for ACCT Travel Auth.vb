Option Explicit On

'Used by Accounting - Travel/Training Authorization
' NOTE: The ID mask character "E" must be passed manually as OnBase does not apply the mask until the keyword value is saved. 
'       The Empl ID # keyword can only be used in a Unity form.
Dim FM_Conn
Dim FM_CMD
Dim FM_RST

Sub OracleClose()
FM_Conn.Close
Set FM_Conn = Nothing
Set FM_CMD = Nothing
End Sub

Sub OracleConnect()

FM_Conn.ConnectionString = "Driver={Microsoft ODBC for Oracle}; " & _
            "CONNECTSTRING=(DESCRIPTION=" & _
            "(ADDRESS=(PROTOCOL=TCP)" & _
            "(HOST=172.16.36.42)(PORT=1521))" & _
            "(CONNECT_DATA=(SERVICE_NAME=ggbprod)));uid=ggbro;pwd=ifas13ro;"

FM_Conn.Open
FM_CMD.ActiveConnection = FM_Conn

End Sub

Sub GetKeywordSetRecords(PrimaryValue, KeysetDef, Results)

set FM_Conn = createobject("ADODB.Connection")
set FM_CMD = createobject("ADODB.Command")
set FM_RST = createobject("ADODB.Recordset")
OracleConnect

   FM_CMD.CommandText = "select e.id,name,substr(d.gl_key,1,3),long_desc,substr(d.gl_key,1,6)," & _
			 "phone_cd,phone_cd2,phone_no,phone_no2" & _
			 " from hr_empmstr e,hr_emppay p,hr_earndist d,hr_pcntble pcn" & _
			 " where e.id='" & ucase(PrimaryValue) & "'" & _
			 " and p.pay_beg<=sysdate and p.pay_end>=sysdate" & _
			 " and e.id=p.id" & _
			 " and p.uniqueid=d.uniqueid" & _
			 " and p.position=pcn.position" & _
			 " and p.pcn=pcn.pcn"
    Set FM_RST = FM_CMD.Execute
    if FM_RST.eof then
	Call Results.beginrow
	Call Results.adddata("EmplID #",trim(PrimaryValue))
	call Results.adddata("Employee Name","")
	call Results.adddata("Department #","")
	call Results.adddata("Position Title","")
	call Results.adddata("GL Key","")
	call Results.adddata("Phone Number","")
	call Results.endrow
   else
    	Call Results.beginrow
	Call Results.adddata("EmplID #",trim(PrimaryValue))
	call Results.adddata("Employee Name",trim(FM_RST.fields(1)))
	call Results.adddata("Department #",trim(FM_RST.fields(2)))
	call Results.adddata("Position Title",trim(FM_RST.fields(3)))
	call Results.adddata("GL Key",trim(FM_RST.fields(4)))
	if trim(FM_RST.fields(5))="OF" then
		call Results.adddata("Phone Number",trim(FM_RST.fields(7)))
	elseif trim(FM_RST.fields(6))="OF" then
		call Results.adddata("Phone Number",trim(FM_RST.fields(8)))
	else
		call Results.adddata("Phone Number","")
	end if
   	Call Results.endrow
   end if
FM_RST.Close
set FM_RST=nothing

OracleClose

End Sub

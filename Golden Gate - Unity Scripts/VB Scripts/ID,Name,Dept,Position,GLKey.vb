Option Explicit On

'Used by Accounting - Cell Phone Assignment Form
Dim FM_Conn
Dim FM_CMD
Dim FM_RST

Sub OracleClose()
FM_Conn.Close
Set FM_Conn = Nothing
Set FM_CMD = Nothing
End Sub

Sub OracleConnect()

FM_Conn.ConnectionString = "Driver={Microsoft ODBC for Oracle}; " & _
            "CONNECTSTRING=(DESCRIPTION=" & _
            "(ADDRESS=(PROTOCOL=TCP)" & _
            "(HOST=172.16.36.42)(PORT=1521))" & _
            "(CONNECT_DATA=(SERVICE_NAME=ggbprod))); uid=bsi;pwd=kam;"

FM_Conn.Open
FM_CMD.ActiveConnection = FM_Conn

End Sub

Sub GetKeywordSetRecords(PrimaryValue, KeysetDef, Results)

set FM_Conn = createobject("ADODB.Connection")
set FM_CMD = createobject("ADODB.Command")
set FM_RST = createobject("ADODB.Recordset")
OracleConnect

    FM_CMD.CommandText = "select e.id,name,substr(d.gl_key,1,3),long_desc,substr(d.gl_key,1,6)" & _
			 " from hr_empmstr e,hr_emppay p,hr_earndist d,hr_pcntble pcn" & _
			 " where e.id='" & PrimaryValue & "'" & _
			 " and p.calc_beg<=sysdate and p.calc_end>=sysdate" & _
			 " and e.id=p.id" & _
			 " and p.uniqueid=d.uniqueid" & _
			 " and p.position=pcn.position" & _
			 " and p.pcn=pcn.pcn" & _
			 " and stat='A' and hr_status='A'"
    Set FM_RST = FM_CMD.Execute
    if FM_RST.eof then
	Call Results.beginrow
	Call Results.adddata("Employee ID #",trim(PrimaryValue))
	call Results.adddata("Employee Name","")
	call Results.adddata("Department #","")
	call Results.adddata("Position Title","")
	call Results.adddata("GL Key","")
	call Results.endrow
    else
    	Call Results.beginrow
	Call Results.adddata("Employee ID #",trim(PrimaryValue))
	call Results.adddata("Employee Name",trim(FM_RST.fields(1)))
	call Results.adddata("Department #",trim(FM_RST.fields(2)))
	call Results.adddata("Position Title",trim(FM_RST.fields(3)))
	call Results.adddata("GL Key",trim(FM_RST.fields(4)))
    	Call Results.endrow
    end if

FM_RST.Close
set FM_RST=nothing

OracleClose

End Sub
